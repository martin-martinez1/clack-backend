package main

import (
	"encoding/json"
	"log"
	"time"

	"github.com/fasthttp/websocket"
)

// Ping is to know if a connection has to be kept alive
var (
	pongwait = 10 * time.Second

	pingInterval = (pongwait * 9) / 10
)

type ClientList map[*Client]bool

type Client struct {
	hub        *Hub
	connection *websocket.Conn

	chatroom string
	// outputChannel     chan []byte
	outputChannel chan Event
}

func NewClient(conn *websocket.Conn, hub *Hub) *Client {
	return &Client{
		connection: conn,
		hub:        hub,

		// outputChannel is used to avoid concurrent writes on the websocket connection
		outputChannel: make(chan Event),
	}
}

func (c *Client) readMessages() {
	defer func() {
		// Removes the client when the for loop ends
		c.hub.unregisterClient(c)
	}()

	// Set how long to wait for the message response
	if err := c.connection.SetReadDeadline(time.Now().Add(pongwait)); err != nil {
		log.Println(err)
	}

	// Max size of the message in bytes
	c.connection.SetReadLimit(512)

	c.connection.SetPongHandler(c.pongHandler)

	for {
		_, payload, err := c.connection.ReadMessage()

		if err != nil {

			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				// Would handle the error in a real app
				log.Printf("error reading message: %v ", err)
			}
			break

		}

		var request Event

		if err := json.Unmarshal(payload, &request); err != nil {
			log.Printf("error unmarshalling event: %v", err)
			// break
		}

		if err := c.hub.routeEvent(request, c); err != nil {
			log.Printf("error handling message: %v", err)
		}
	}
}

func (c *Client) writeMessages() {
	defer func() {
		c.hub.unregisterClient(c)
	}()

	ticker := time.NewTicker(pingInterval)

	for {
		select {
		case message, ok := <-c.outputChannel:
			if !ok {
				if err := c.connection.WriteMessage(websocket.CloseMessage, nil); err != nil {
					log.Println("connection closed: ", err)

				}
				return
			}

			data, err := json.Marshal(message)
			if err != nil {
				// should handle error than continue
				log.Println(err)
				return
			}

			if err := c.connection.WriteMessage(websocket.TextMessage, data); err != nil {
				log.Printf("falied to send message: %v", err)
			}

		case <-ticker.C:
			log.Println("ping")

			// Send ping to client
			// Normally the browser sends the pong response automatically
			if err := c.connection.WriteMessage(websocket.PingMessage, []byte("")); err != nil {
				log.Println("writemesg err: ", err)
				return
			}

		}
	}
}

func (c *Client) pongHandler(pongMsg string) error {
	log.Println("pong")
	return c.connection.SetReadDeadline(time.Now().Add(pongwait))
}
