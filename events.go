package main

import (
	"encoding/json"

	"github.com/google/uuid"
)

// Limit the event that are handled
type Event struct {
	Type    string          `json:"type"`
	Payload json.RawMessage `json:"payload"`
}

// Add a uuid to the message
// Add a event type to tell if the message was an edit, new message, or deletion

type EventHandler func(event Event, c *Client, h *Hub) error

const (
	EventSendMessage      = "send_message"
	EventNewMessage       = "new_message"
	EventEditMessage      = "edit_message"
	EventDeleteMessage    = "delete_message"
	EventBroadcastMessage = "broadcast_message"
)

type MessageStatus string

const (
	Normal  MessageStatus = "normal"
	Edited  MessageStatus = "edited"
	Deleted MessageStatus = "deleted"
)

// This is recieveced and converted to on of the types below
type NewMessage struct {
	Message string `json:"message"`
	From    string `json:"from"`
}

type ExistingMessage struct {
	Id             uuid.UUID     `json:"id"`
	Status         MessageStatus `json:"status"`
	LastChangeDate string        `json:"lastChangeDate"`
	Message        string        `json:"message"`
	From           string        `json:"from"`
	Sent           string        `json:"sent"`
}

// These are sent out
type BroadcastMessageEvent struct {
	Type           string        `json:"type"`
	Id             uuid.UUID     `json:"id"`
	Status         MessageStatus `json:"status"`
	LastChangeDate string        `json:"lastChangeDate"`
	Message        string        `json:"message"`
	From           string        `json:"from"`
	Sent           string        `json:"sent"`
}

type EditMessageEvent struct {
	Type           string        `json:"type"`
	Id             uuid.UUID     `json:"id"`
	Status         MessageStatus `json:"status"`
	LastChangeDate string        `json:"lastChangeDate"`
	NewText        string        `json:"newText"`
	Message        string        `json:"message"`
	From           string        `json:"from"`
	Sent           string        `json:"sent"`
}

type DeleteMessageEvent struct {
	Type   string        `json:"type"`
	Id     uuid.UUID     `json:"id"`
	Status MessageStatus `json:"status"`
	From   string        `json:"from"`
	Sent   string        `json:"sent"`
}
