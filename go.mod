module martmartez/todos

go 1.19

require (
	github.com/fasthttp/websocket v1.5.4
	github.com/google/uuid v1.3.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/savsgio/gotils v0.0.0-20230208104028-c358bd845dee // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.48.0 // indirect
	golang.org/x/sync v0.3.0
)
