package main

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/google/uuid"
)

// Maybe move it later on?
type Clock interface {
	Now() time.Time
	// After(d time.Duration) <-chan time.Time
}
type IdService interface {
	New() uuid.UUID
}

type Hub struct {
	// Keep Track of connections
	clients ClientList
	// Protect it using a read write mutex
	sync.RWMutex
	// Keep Track of handlers
	handlers map[string]EventHandler

	// I need a way to stub time.Now inside of tests and this is the way to do it according to the internet
	// Although making changes to the code to be able to make the tests work seems wrong.
	clock Clock

	idService IdService
}

func NewHub(ctx context.Context, clock Clock, idService IdService) *Hub {
	hub := &Hub{
		clients:   make(ClientList),
		handlers:  make(map[string]EventHandler),
		clock:     clock,
		idService: idService,
	}

	hub.setupEventHandlers()
	return hub
}

func (h *Hub) setupEventHandlers() {
	h.handlers[EventSendMessage] = SendMessage
	h.handlers[EventBroadcastMessage] = SendMessage
	h.handlers[EventEditMessage] = EditMessage
	h.handlers[EventDeleteMessage] = DeleteMessage
}

// Pass in hub's clock struct
func (h *Hub) routeEvent(event Event, c *Client) error {
	// Check if the event type

	if handler, ok := h.handlers[event.Type]; ok {
		if err := handler(event, c, h); err != nil {
			return err
		}
		return nil
	} else {
		return errors.New("there is no such event type")
	}
}

// registering and unregistering can also be a chan
func (h *Hub) registerClient(client *Client) {
	h.Lock()
	defer h.Unlock()
	h.clients[client] = true

}

func (h *Hub) unregisterClient(client *Client) {
	h.Lock()
	defer h.Unlock()

	if _, ok := h.clients[client]; ok {
		client.connection.Close()
		delete(h.clients, client)
	}
}
