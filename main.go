package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
)

func main() {
	log.Println("Starting...")
	startWebSocketServer()

	log.Fatal(http.ListenAndServe(":8080", nil))
}

type realClock struct{}

func (r realClock) Now() time.Time {
	return time.Now()
}

type uuidService struct{}

func (u uuidService) New() uuid.UUID {
	return uuid.New()
}

// func (r realClock) After(d time.Duration) <-chan time.Time { return time.After(d) }

func startWebSocketServer() {
	ctx := context.Background()

	clock := realClock{}
	idService := uuidService{}

	hub := NewHub(ctx, clock, idService)

	// http.Handle("/", http.FileServer(http.Dir("./frontend")))
	http.HandleFunc("/ws", hub.ServeHTTP)
	// http.HandleFunc("/login", hub.loginHandler)

}
