package main

import (
	"encoding/json"
	"fmt"
	"log"
)

// Call with a clock struct, hub should pass in its clock struct
func SendMessage(event Event, c *Client, h *Hub) error {

	var chatMessage NewMessage

	if err := json.Unmarshal(event.Payload, &chatMessage); err != nil {
		log.Println(err)
		return fmt.Errorf("bad payload in request: %v", err)
	}

	// Broadcast data to other clients
	// Extract it into its own function later
	var broadMessage BroadcastMessageEvent

	broadMessage.Id = h.idService.New()
	broadMessage.Status = Normal
	broadMessage.Type = EventNewMessage
	broadMessage.Sent = h.clock.Now().Format("2006-01-02 15:04:05 MST")
	broadMessage.Message = chatMessage.Message
	broadMessage.From = chatMessage.From

	data, err := json.Marshal(broadMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	outgoingEvent := Event{
		Payload: data,
		Type:    EventNewMessage,
	}

	for client := range c.hub.clients {
		if client.chatroom == c.chatroom {

			client.outputChannel <- outgoingEvent
		}
	}

	return nil
}

func EditMessage(event Event, c *Client, h *Hub) error {

	var chatMessage EditMessageEvent

	if err := json.Unmarshal(event.Payload, &chatMessage); err != nil {
		log.Println(err)
		return fmt.Errorf("bad payload in request: %v", err)
	}

	var broadMessage BroadcastMessageEvent

	broadMessage.Id = chatMessage.Id
	broadMessage.Type = EventEditMessage
	broadMessage.LastChangeDate = h.clock.Now().Format("2006-01-02 15:04:05 MST")
	broadMessage.Status = Edited
	broadMessage.Sent = chatMessage.Sent
	broadMessage.Message = chatMessage.NewText
	broadMessage.From = chatMessage.From

	data, err := json.Marshal(broadMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	outgoingEvent := Event{
		Payload: data,
		Type:    EventEditMessage,
	}

	for client := range c.hub.clients {
		if client.chatroom == c.chatroom {

			client.outputChannel <- outgoingEvent
		}
	}

	return nil

}
func DeleteMessage(event Event, c *Client, h *Hub) error {

	var chatMessage DeleteMessageEvent

	if err := json.Unmarshal(event.Payload, &chatMessage); err != nil {
		log.Println(err)
		return fmt.Errorf("bad payload in request: %v", err)
	}

	var broadMessage BroadcastMessageEvent

	broadMessage.Id = chatMessage.Id
	broadMessage.Type = EventDeleteMessage
	broadMessage.LastChangeDate = h.clock.Now().Format("2006-01-02 15:04:05 MST")
	broadMessage.Status = Deleted
	broadMessage.Sent = chatMessage.Sent
	broadMessage.Message = ""
	broadMessage.From = chatMessage.From

	data, err := json.Marshal(broadMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	outgoingEvent := Event{
		Payload: data,
		Type:    EventDeleteMessage,
	}

	for client := range c.hub.clients {
		if client.chatroom == c.chatroom {

			client.outputChannel <- outgoingEvent
		}
	}

	return nil

}

func BroadcastMessage(event Event, c *Client, h *Hub) error {
	var broadcastEvent BroadcastMessageEvent

	if err := json.Unmarshal(event.Payload, &broadcastEvent); err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	var broadcastMessage BroadcastMessageEvent

	broadcastMessage.Sent = h.clock.Now().Format("2006-01-02 15:04:05 MST")
	broadcastMessage.Message = broadcastEvent.Message
	broadcastMessage.From = broadcastEvent.From

	data, err := json.Marshal(broadcastMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	outgoingEvent := Event{
		Payload: data,
		Type:    EventNewMessage,
	}

	for client := range c.hub.clients {
		if client.chatroom == c.chatroom {

			client.outputChannel <- outgoingEvent
		}
	}

	return nil

}
