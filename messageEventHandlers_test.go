package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/fasthttp/websocket"
	"github.com/google/uuid"
)

func newWSServer(t *testing.T, h http.Handler) (*httptest.Server, *websocket.Conn) {
	t.Helper()

	s := httptest.NewServer(h)
	wsURL := strings.Replace(s.URL, "http", "ws", -1)

	ws, _, err := websocket.DefaultDialer.Dial(wsURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	return s, ws
}

type testClock struct{}

func (t testClock) Now() time.Time {
	date, err := time.Parse("2006-01-02 15:04:05 MST", "2023-08-23 07:13:14 MST")
	if err != nil {
		log.Println(err.Error())
		return time.Time{}
	}
	return date
}
func (t testClock) After(d time.Duration) <-chan time.Time { return time.After(d) }

type testIdService struct{}

func (id testIdService) New() uuid.UUID {
	return uuid.MustParse("487e0f13-8800-4225-bdcd-b61adba37d6d")
}

func TestSendMessage(t *testing.T) {

	type testCase struct {
		name     string
		event    Event
		expected json.RawMessage
	}

	testCases := []testCase{

		{
			name: "test1",
			event: Event{
				Type:    "send_message",
				Payload: json.RawMessage(`{"message":"Hello guys how are you doing", "From":"L33t"}`),
			},
			expected: json.RawMessage(`{"type":"new_message","payload":{"type":"new_message","id":"487e0f13-8800-4225-bdcd-b61adba37d6d","status":"normal","lastChangeDate":"","message":"Hello guys how are you doing","from":"L33t","sent":"2023-08-23 07:13:14 MST"}}`),
		},
		{
			name: "test2",
			event: Event{
				Type:    "send_message",
				Payload: json.RawMessage(`{"message":"stuff2", "From":"Jack"}`),
			},
			expected: json.RawMessage(`{"type":"new_message","payload":{"type":"new_message","id":"487e0f13-8800-4225-bdcd-b61adba37d6d","status":"normal","lastChangeDate":"","message":"stuff2","from":"Jack","sent":"2023-08-23 07:13:14 MST"}}`),
		},
	}

	ctx := context.Background()

	clock := testClock{}

	stubIdGen := testIdService{}

	hub := NewHub(ctx, clock, stubIdGen)

	hub.setupEventHandlers()
	s, ws := newWSServer(t, hub)
	client := NewClient(ws, hub)

	defer s.Close()
	defer ws.Close()
	hub.registerClient(client)

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {

			errors := make(chan error)

			go func() {
				err := SendMessage(tt.event, client, hub)
				if err != nil {
					log.Println("error")
					errors <- err
				}
			}()

			select {
			case errs := <-errors:
				if errs != nil {

					t.Fatal(errs)
				}
			case message := <-client.outputChannel:
				data, err := json.Marshal(message)
				if err != nil {
					t.Fatal("problem marshaling")
				}
				// Loop to check equality
				for index := range data {
					if data[index] != tt.expected[index] {
						t.Fatalf("expected %v but got %v different at %v", string(tt.expected), string(data), index)

					}
				}
			}

		})
	}

}

func TestEditMessage(t *testing.T) {

	type testCase struct {
		name     string
		event    Event
		expected json.RawMessage
	}

	testCases := []testCase{

		{
			name: "test1",
			event: Event{
				Type:    "edit_message",
				Payload: json.RawMessage(`{"status":"normal","from":"l33t","message":"asdasd","sent":"2023-08-23 07:13:14 MST","id":"487e0f13-8800-4225-bdcd-b61adba37d6d","lastChangeDate":"","newText":"Hello guys how are you doing"}`),
			},
			expected: json.RawMessage(`{"type":"edit_message","payload":{"type":"edit_message","id":"487e0f13-8800-4225-bdcd-b61adba37d6d","status":"edited","lastChangeDate":"2023-08-23 07:13:14 MST","message":"Hello guys how are you doing","from":"l33t","sent":"2023-08-23 07:13:14 MST"}} `),
		},
	}

	ctx := context.Background()

	clock := testClock{}

	stubIdGen := testIdService{}

	hub := NewHub(ctx, clock, stubIdGen)

	hub.setupEventHandlers()
	s, ws := newWSServer(t, hub)
	client := NewClient(ws, hub)

	defer s.Close()
	defer ws.Close()
	hub.registerClient(client)

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {

			errors := make(chan error)

			go func() {
				err := EditMessage(tt.event, client, hub)
				if err != nil {
					log.Println("error")
					errors <- err
				}
			}()

			select {
			case errs := <-errors:
				if errs != nil {

					t.Fatal(errs)
				}
			case message := <-client.outputChannel:
				data, err := json.Marshal(message)
				if err != nil {
					t.Fatal("problem marshaling")
				}
				// Loop to check equality
				for index := range data {
					if data[index] != tt.expected[index] {
						t.Fatalf("expected %v but got %v different at %v", string(tt.expected), string(data), index)

					}
				}
			}

		})
	}

}

func TestDeleteMessage(t *testing.T) {

	type testCase struct {
		name     string
		event    Event
		expected json.RawMessage
	}

	testCases := []testCase{

		{
			name: "test1",
			event: Event{
				Type:    "delete_message",
				Payload: json.RawMessage(`{"status":"normal", "sent":"2023-08-23 07:13:14 MST","id":"487e0f13-8800-4225-bdcd-b61adba37d6d"}`),
			},
			expected: json.RawMessage(`{"type":"delete_message","payload":{"type":"delete_message","id":"487e0f13-8800-4225-bdcd-b61adba37d6d","status":"deleted","lastChangeDate":"2023-08-23 07:13:14 MST","message":"","from":"","sent":"2023-08-23 07:13:14 MST"}} `),
		},
	}

	ctx := context.Background()

	clock := testClock{}

	stubIdGen := testIdService{}

	hub := NewHub(ctx, clock, stubIdGen)

	hub.setupEventHandlers()
	s, ws := newWSServer(t, hub)
	client := NewClient(ws, hub)

	defer s.Close()
	defer ws.Close()
	hub.registerClient(client)

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {

			errors := make(chan error)

			go func() {
				err := DeleteMessage(tt.event, client, hub)
				if err != nil {
					log.Println("error")
					errors <- err
				}
			}()

			select {
			case errs := <-errors:
				if errs != nil {

					t.Fatal(errs)
				}
			case message := <-client.outputChannel:
				data, err := json.Marshal(message)
				if err != nil {
					t.Fatal("problem marshaling")
				}
				// Loop to check equality
				for index := range data {
					if data[index] != tt.expected[index] {
						t.Fatalf("expected %v but got %v different at %v", string(tt.expected), string(data), index)

					}
				}
			}

		})
	}

}
