package main

import (
	"log"
	"net/http"

	"github.com/fasthttp/websocket"
)

var (
	websocketUpgrader = websocket.Upgrader{
		CheckOrigin:     checkOrigin,
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

func (h *Hub) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	log.Println("Connecting..")
	conn, err := websocketUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)

	}
	log.Println("Connected")

	client := NewClient(conn, h)

	h.registerClient(client)

	// Start client processes
	go client.readMessages()
	go client.writeMessages()

}

// CORS
// True allow the connection
func checkOrigin(r *http.Request) bool {
	// origin := r.Header.Get("Origin")

	// switch origin {
	// case "http://localhost:8081":
	// 	return true
	// case "http://localhost:808":
	// 	return true
	// default:
	// 	return false
	// }

	return true
}
